#include "types.h"
#include "stat.h"
#include "user.h"
#include "pstat.h"

int main(int argc, char *argv[])
{
  int i;
  int fork_res;
  printf(1, "Parent PID: %d\n", getpid());
  for (i = 1; i <= 3; i++)
  {
    fork_res = fork();
    if (fork_res == 0)
    {
      settickets(i * 10);
      //Child process
      printf(1,
	     "Child %d PID: %d\n",
	     i,
	     getpid());
      for(;;){}
    }
  }
  exit();
}
